package com.example.notes

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.renderscript.ScriptGroup.Binding
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import com.example.notes.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        loadData()

        binding.apply {
            names.text = getContacts()
            val size = (getContacts()?.lines()?.size!! - 1) / 3
            names.setOnClickListener {
                saveData()
            }

            note1.isVisible = 1 <= size
            note2.isVisible = 2 <= size
            note3.isVisible = 3 <= size
            note4.isVisible = 4 <= size
            note5.isVisible = 5 <= size
            note6.isVisible = 6 <= size
            note7.isVisible = 7 <= size
        }

        setContentView(binding.root)
    }

    @SuppressLint("Range")
    private fun getContacts(): String? {
        var list = ""
        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                list += name.toString()
                for (i in 0..2) list += "\n"
            } while (cursor.moveToNext())
            cursor.close()
            return list
        }
        return null
    }

    private fun saveData() {
        val insertedText = listOf(
            binding.note1.text.toString(),
            binding.note2.text.toString(),
            binding.note3.text.toString(),
            binding.note4.text.toString(),
            binding.note5.text.toString(),
            binding.note6.text.toString(),
            binding.note7.text.toString()
        )

        val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
        val editor = sharedPr.edit()
        editor.apply {
            putString("a", insertedText[0])
            putString("b", insertedText[1])
            putString("c", insertedText[2])
            putString("d", insertedText[3])
            putString("e", insertedText[4])
            putString("f", insertedText[5])
            putString("g", insertedText[6])
        }.apply()
    }

    private fun loadData() {
        val sharedPr = getSharedPreferences("sharedPr", Context.MODE_PRIVATE)
        binding.apply {
            note1.setText(sharedPr.getString("a", null))
            note2.setText(sharedPr.getString("b", null))
            note3.setText(sharedPr.getString("c", null))
            note4.setText(sharedPr.getString("d", null))
            note5.setText(sharedPr.getString("e", null))
            note6.setText(sharedPr.getString("f", null))
            note7.setText(sharedPr.getString("g", null))
        }
    }

}